# app-env-conf.js

#### 项目介绍

node.js 应用程序环境配置管理类

#### 安装教程

`npm install app-env-conf --save`

`yarn add app-env-conf`

#### 使用说明

推荐在实际的项目中，添加一个 config.js，输出当前项目的 AppConf 唯一实例，而不要在源代码中创建多个 `new AppConf()` 实例来使用。

```javascript
// src/config.js

import AppConf from 'app-env-conf';

const appConf = new AppConf();

export default appConf.config;
```

抑或对 AppConf 部分属性和方法进行重载

```javascript
// src/config.js

import AppConf from 'app-env-conf';

class YourAppConf extends AppConf {
	envFile = '.env';

	getConfigFiles() {
		return [
			/* ... 特定的项目配置环境路径组 */
		];
	}
}

const appConf = new YourAppConf();

export default appConf.config;
```

在其他的 module 中加载 config.js

```javascript
// src/others.js

import config from './config';
```

`appConf.config` 属性实际上为 AppConf 实例的 getter 方法，主要用于延迟实际加载配置的时机。
