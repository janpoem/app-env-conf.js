import fs from 'fs';
import pathUtil from 'path';
import AppConf from '../index';

describe('AppEnv测试', () => {
	const sampleApp = './sample_app';
	const sampleAppPath = pathUtil.resolve(__dirname, sampleApp);

	const testEnv = 'production';

	const random = Math.random();
	const anyValue = 'anything_' + random,
		value1 = 'value_' + random;
	const confData = {
		anything: anyValue,
		key1: value1,
	};

	function writeFileSync(path, data) {
		fs.writeFileSync(path, JSON.stringify(confData));
	}

	try {
		// 写入配置文件，以确保每次测试都是执行重新生成的配置值
		// 将 confData 写入 config/production.json
		const confFile = pathUtil.resolve(sampleAppPath, 'test_config.json');
		writeFileSync(confFile, confData);

		// 初始化一个项目配置实例
		const conf = new AppConf(sampleAppPath, testEnv, confFile);

		console.log(conf.config);

		it('conf.root', () => {
			expect(conf.root).toEqual(sampleAppPath);
		});

		it('conf.env', () => {
			expect(conf.env).toEqual(testEnv);
		});

		it('conf.path()', () => {
			let path = 'test_dir';
			let testPath = pathUtil.resolve(sampleAppPath, path);
			expect(conf.path(path)).toEqual(testPath);
		});

		it('test config data', () => {
			expect(conf.config.anything).toEqual(anyValue);
			expect(conf.config.key1).toEqual(value1);
		});
	} catch (err) {
		console.error(err);
	}
	// console.log(conf.config);
});
